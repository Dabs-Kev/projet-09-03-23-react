# Projet Fullstack avec ReactJS, ExpressJS et MongoDB

## Version: 1.0.0

## Lancement du projet

Après avoir cloner le dépot localement, vous trouverez deux dossier:

## back

L'application avec ExpressJS, qui servira d'api pour le site pour authentifier et gérer les posts.

Avant de la lancer, il faut installer les dépendances. ouvrez un nouveau terminal dans le dossier back et lancez cette commande:

```bash
npm install
```

Puis vous pouvez lancer l'application avec cette commande:

```bash
npm run dev
```

## front

L'application avec ReactJS, qui servira de front. Elle est servie avec webpack en mode dévelopement.

Avant de la lancer, il faut installer les dépendances. ouvrez un nouveau terminal dans le dossier front et lancez cette commande:

```bash
npm install
```

Puis vous pouvez lancer l'application avec cette commande:

```bash
npm start
```

## Déscription

Le site est un réseau social ou les utilisateurs peuvent lire des posts créés par d'autre utilisateurs sur la page d'acceuil.

Les utilisateurs peuvent s'authentifier avec un email, un mot de passe et un pseudo (inscription puis connexion), et de manière persistante en utilisant les JWT token et lo localStorage ou les coockies.

Une fois authetifié, les utilisateurs peuvent accéder à leur page de profil ou sera affiché:

- Son pseudo et son image de profil qu'il pourra modifier.
- Un formulaire pour créer des posts avec un titre et du contenu, et une image optionnelle.
- La liste des posts de l'utilisateur (titre, coontenu et image) ainsi qu'un bouton pour les supprimer.

Le site doit avoir une Navbar responsive pour naviguer entre les pages. Dans la Navbar il y'aura un lien vers la page d'accueil, et suivant:

- si l'utilisateur est connecté un lien vers la page de profil, et un lien pour se deconnecter.
- Si l'utilisateur n'est pas connecté, un lien vers la page d'authentificaiton.

Et un footer à la fin de chaques pages.

---

Créer un dépot GitHub/GitLab, Fair un premier push sur la branche main, et inviter djemai-samy sur le dépot.

---

## Les Etapes

## Etape 1: Initialisation du projet ***TERMINÉ***

---

### 1.1 Initialiser un projet avec ExpressJS

1. Initialiser un projet avec la commande `npm init`
2. Installer les dépendances: express, mongoose, morgan, jsonwebtoken, bcrypt, multer
3. Installer les dépendances de developpeur: nodemon
4. Créer le script principale à lancer dans un fichier nommée: app.js:
    - Le script lance le serveur sur le port 3001.
    - Mettre en place une route simple pour tester si ça fonctionne.
5. Ajouter des scripts au package.json pour automatiser le lancement du serveur.

---

### 1.2 Initialiser un projet avec ReactJS

1. Utiliser le script create-react-app pour initialiser un projet react: `npx create-react-app my-app`.
2. Installer les dépendances: react-router-dom, axios.
3. Ajouter le proxy au package.json pour pointer vers la backend.
4. Faire une requête vers la backend en utilisant useEffect, pour tester si ça fonctionne.
5. Lancer l'application avec la commande: `npm start`.

---

## Etape 2: Structure de la front-end ***TERMINÉ***

## 1.1 Créer les composant pour les différentes pages

- Home.jsx pour la page d'acceuil sur la route: '/'
- Auth.jsx pour la page d'authentification sur la route: /auth
- Profil.jsx pour la page de profil sur la route: /profil

---

## 1.2 Mettre en place la navigation

1. Dans le composant App.jsx, utiliser les BrowserRouter dans App.jsx pour mettre en place les différentes page.
2. Mettre en place le routing des pages dans Main.jsx, et placer le composant dans App.jsx.
3. Créer le composant Navbar.jsx pour permettre à l'utilisateur de changer de page en utilisant le composant Link., et afficher les Navbar dans App.jsx
4. Créer le composant Footer.jsx et l'afficher dans App.jsx.

---

## 1.3 Mettre en place le contexte

1. Créer une composant Provider qui:
    1. Créer un contexte pour partager les données de l'utilisateur aux enfants.
    2. Intialiser une variable d'état pour sotcker et modifié les données de l'utilisateur.
    3. Mettre la varible d'état dans le contexte.
    4. Afficher les enfants avec la props children.
2. L'utiliser dans le composant prnicipale App.jsx

---

## Etape 3: Authentification Dans la Backend ***TERMINÉ***

### 3.1: Les routes

1. Inscription de l'utilisateur. La fonction reçoit l'email, le username et le mot de passe dans le corps de la requête.
  
- **URL**: /users/signup
- **Méthode**: POST
- **Body**:
  - **email** : l'email de l'utilisateur à ajouter (obligatoire).
  - **username** : le nom d'utilisateur de l'utilisateur à ajouter (obligatoire).
  - **password** : le mot de passe de l'utilisateur à ajouter (obligatoire).
- **Réponses**:
  - **Code 201** : succès. L'utilisateur a été ajouté.

    ```json
    {
      "message": "user added"
    }
    ```

  - **Code 400** : erreur. L'email est manquant dans la requête.

    ```json
      {
        "message": "Email required"
      }
      ```

  - **Code 400** : erreur. Le mot de passe est manquant dans la requête.

    ```json
      {
        "message": "Password required"
      }
      ```

  - **Code 400** : erreur. message: le username est manquant dans la requête.

    ```json
      {
        "message": "Username required"
      }
    ```

---

2. Connexion de l'utilisateur: Le client fournit dans le corps de la requête un email et un mot de passe.

- **URL**: /users/login
- **Méthode**: POST
- **Body** :
  - **email** : l'email de l'utilisateur à connecter (obligatoire).
  - **password** : le mot de passe de l'utilisateur à connecter (obligatoire).
- **Réponses** :
  - **Code 200** : succès la réponse contient les informations de l'utilisateur formatées et un token JWT.
  
    ```json
    {
      "user": {
          "id": "640b3e9f8c19b6ab27d0a4f7",
          "email": "john@email.com",
          "username": "John Doe"
      },
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0MGIzZTlmOGMxOWI2YWIyN2QwYTRmNyIsImlhdCI6MTY3ODYzOTM3MCwiZXhwIjoxNjc5MjQ0MTcwfQ.9HrHfb-Gq-sCrCM18YR4A3Uy2QZz-3vcFaba-SZM6V0"
    }

    ```

  - **Code 400** : erreur car l'email est manquant.

    ```json
    {
      "message": "Email required"
    }
    ```

  - **Code 400** : erreur car Le mot de passe est manquant.

    ```json
    {
      "message": "password required"
    }
    ```

  - **Code 400** : erreur car le mode ne correspond pas.

    ```json
    {
      "message": "Invalid credentials"
    }
    ```

  - **Code 404** : erreur car l'email n'existe pas dans la base de donnée.

    ```json
    {
      "message": "Invalid credentials"
    }
    ```

---

3. Récupère les informations d'un utilisateur en utilisant le token et renvoie une réponse formatée.

- **URL**: /users/
- **Méthode**: GET
- **headers** :
  - **Authorization** : le token fournit lors de la connexion (obligatoire).
- **Réponses** :
  - **Code 200** : succès la réponse contient les informations de l'utilisateur formatées.
  
    ```json
    {
      "user": {
          "id": "640b3e9f8c19b6ab27d0a4f7",
          "email": "john@email.com",
          "username": "John Doe"
      }
    }

    ```

  - **Code 401** : erreur car le token n'est pas fourni, expiré ou invalide.

    ```json
    {
      "message": "Unauthorized"
    }
    ```

  - **Code 404** : erreur car l'utilisateur n'existe pas dans le BDD.

  ```json
  {
    "message": "User not found"
  }
  ```

---

4. Récupère les informations d'un utilisateur avec son ID depuis la base de données et renvoie une réponse formatée.

- **URL**: /users/:id
- **Méthode**: GET
- **params** :
  - **id** : l'id de l'utilisateur à récuperer (obligatoire).
- **Réponses** :
  - **Code 200** : succès la réponse contient les informations de l'utilisateur formatées.
  
    ```json
    {
      "user": {
          "id": "640b3e9f8c19b6ab27d0a4f7",
          "email": "john@email.com",
          "username": "John Doe"
      }
    }

    ```

  - **Code 404** : erreur l'utilisateur n'existe pas dans le BDD.

    ```json
    {
      "message": "User not found"
    }
    ```

---

5. Mettre à jour le username de l'utilisateur.

- **URL**: /users/:id
- **Méthode**: PUT
- **headers** :
  - **Authorization** : le jwt token fournit lors de la connexion (obligatoire).
- **params** :
  - **id** : l'id de l'utilisateur à récuperer (obligatoire).
- **body**:
  - **username**: Le nouveau username de l'utilisateur.
- **Réponses** :
  - **Code 200** : succès la réponse contient les informations de l'utilisateur formatées.
  
    ```json
    {
      "user": {
          "id": "640b3e9f8c19b6ab27d0a4f7",
          "email": "john@email.com",
          "username": "Nouveau John Doe"
      }
    }

    ```

---

6. Mettre à jour l'image de profil de l'utilisateur

- **URL**: /users/avatar
- **Méthode**: POST
- **headers** :
  - **Authorization** :le jwt token fournit lors de la connexion (obligatoire).
- **body**:
  - **file**: Le fichier image à enregistrer dans le serveur..
- **Réponses** :
  - **Code 200** : succès la réponse contient les informations de l'utilisateur formatées.
  
    ```json
    {
      "user": {
          "id": "640b3e9f8c19b6ab27d0a4f7",
          "email": "john@email.com",
          "username": "Nouveau John Doe",
          "avatar": "/images/avatar/640b3e9f8c19b6ab27d0a4f7-1678565697444.png"
      }
    }

    ```

---

### 3.2: Le Model

1. Créer une base de données sur MongoDB et la connecter sur le script principale.
2. Créer le schema de l'utilisateur:
    - email: String
    - password: String
    - username: String
    - avatar: String.
3. Créer le model pour User et l'exporter.

### 3.3: Les controlleurs

1. Inscription

    Créer la fonction pour l'inscription:

    - Reçoit dans le body de la requête: email, password et username.
    - Tester si les données existent et sont correctes, sinon retourner des messages d'erreurs dans la réponse.
    - Tester si l'email n'existe pas, retourner une erreur si il existe.
    - Crypter le mot de passe avec la librairie bcrypt
    - Utilise le Model pour ajouter l'utilisateur dans la base de données et repondre avec un message de réussite.

2. Connexion

    Créer la fonction pour la connexion:

    - Reçoit dans le body de la requête: email et password.
    - Tester si les données existent et sont correcte, sinon retourner des message d'erreurs dans la réponse.
    - Verifier si l'email existe, sinon retourner un message d'erreurs.
    - Verifier si le password est correcte avec la librairie bcrypt, sinon retourner un message d'erreur.
    - Créer un JWT token.
    - Retourner les informations de l'utilisateur et le token

3. Récuperer les informations de l'utilisateur

    Créer la fonction pour récuperer les informations grâce au jwt token:

    - Reçoit dans les header le JWT token.
    - Vérifier si le token est bon, sinon retourner un erreur.
    - utiliser le Model pour récuperer l'utilsateur avec son 'id'.
    - Retourner les informations de l'utilisateur.

4. Récuperer les informations de l'utilisateur avec son id

    Créer la fonction pour récuperer les informations grâce a l'id:

    - Vérifier si le token est bon, sinon retourner un erreur.
    - utiliser le Model pour récuperer l'utilsateur avec son 'id'.
    - Retourner les informations de l'utilisateur.

5. Mettre à jour le username de l'utilisateur

    Créer la fonction pour mettre à jour l'utilisateur:

    - Reçoit dans les header le JWT token.
    - Reçoit dans le body le nouveau username
    - Vérifier si le token est bon, sinon retourner un erreur.
    - Vérifier si le username est bon, sinon retourner un erreur.
    - utiliser le Model pour récuperer l'utilsateur avec son 'id'.
    - Mettre à jour le username et sauvegarder l'utilisateur
    - Retourner les informations de l'utilisateur.

6. Uploader l'avatar de l'utilisateur

    Créer la fonction pour Uploader l'avatar

    - Reçoit dans les header le JWT token.
    - Vérifier si le token est bon, sinon retourner un erreur.
    - Utiliser multer pour enregistrer le fichier
    - Reçoit dans le body le nom du fichier
    - Utiliser le Model pour récuperer l'utilsateur avec son 'id'.
    - Mettre à jour l'avatar avec le chemin du fichier
    - Retourner les informations de l'utilisateur.

---

## Etape 4: Authentification Dans la FrontEnd ***TERMINER***

### 4.1 Inscription

Créer un composant pour l'inscription avec:

1. 4 inputs: email, username, password, confirmPassword
2. Un bouton pour valider le formulaire
3. Les variables d'états et leurs setteur pour chaque input
4. Des variables d'états pour les differents message d'erreur à afficher
5. Les handlers pour chauqe input, qui modifient les variables d'états et vide les message d'erreurs.
6. La fonction de validation:
    - Tester les entrées et afficher les message d'erreurs.
    - Utiliser axios pour envoyer la requete vers la backend, et gérer la réponse.

---

### 4.2 Connexion

Créer un composant pour la connexion avec:

1. 2 inputs: email et password
2. Un bouton pour valider le formulaire
3. Les variables d'états et leurs setteur pour chaque input
4. Des variables d'états pour les differents message d'erreur à afficher
5. Les handlers pour chaque input, qui modifient les variables d'états et vide les message d'erreurs.
6. La fonction de validation:
    - Tester les entrées et afficher les message d'erreurs.
    - Utiliser axios pour envoyer la requete vers la backend, et gérer la réponse.
    - Mettre a jour le contexte avec les données de l'utilisateur
    - Mettre le token dans le localStorage.

---

### 4.3 Persistance le l'authentification

Utiliser un useEffect dans le Provider, pour aller chercher les données de l'utilisateur quand l'application est rendu la premiere fois.

---

### 4.4 Page de profil

1. Afficher les informations de l'utilisateur dans la page de profil.
2. Ajouter la possibilité a l'utilisateur de modifier son username.
3. Ajouter la possibilité a l'utilisateur d'ajouter ou de mettre a jour son avatar.

---

## Etape 5: Les articles dans la backend ***TERMINER***

### 5.1 Les routes

1. Ajouter un article

- **URL**: /posts/post
- **Méthode**: POST
- **Headers**:
  - **Authorization**: le token d'authentification pour l'utilisateur
- **Body**:
  - **title** : le titre de l'article à ajouter (obligatoire).
  - **content** : lede l'article à ajouter (obligatoire).
  - **file** : Le fichier image de l'article (optionnel).
- **Réponses**:
  - **Code 200** : succès. L'article a été ajouté.

    ```json
    {
      "user": {
          "id": "640b3e9f8c19b6ab27d0a4f7",
          "email": "john@email.com",
          "username": "John Doe",
          "avatar": "/images/avatar/640b3e9f8c19b6ab27d0a4f7-1678565697344.png",
          "posts": [
            {
              "id": "640dssdcdd6d2dd0qsf7",
              "title":"Le titre de l'article",
              "content":"Le contenu de l'article",
              "image": "/images/post/640dssdcdd6d2dd0qsf7-1678565697344.jpg",
              "createdAt": "2023-03-11T20:41:50.245Z",
              "updatedAt": "2023-03-11T20:41:50.245Z",
              "user": {
                 "id": "640b3e9f8c19b6ab27d0a4f7"
              }
            }
          ]
      }
    }
    ```

  - **Code 400** : erreur. Le titre est manquant.

    ```json
    {
      "message": "Title required"
    }
    ```

  - **Code 400** : erreur. Le contenu est manquant.

    ```json
    {
      "message": "Content required"
    }
    ```

  ---

  2. Supprimer un article

  - **URL**: /posts/:id
  - **Méthode**: DELETE
  - **Headers**:
    - **Authorization**: le token d'authentification pour l'utilisateur
  - **Params**:
    - **id** : L'ID de l'article à supprimer (obligatoire).
  - **Réponses**:
    - **Code 200** : succès. L'article a été supprimer.

      ```json
      {
        "user": {
            "id": "640b3e9f8c19b6ab27d0a4f7",
            "email": "john@email.com",
            "username": "John Doe",
            "avatar": "/images/avatar/640b3e9f8c19b6ab27d0a4f7-1678565697344.png",
            "posts": []
        }
      }
      ```

---

  3. Récuprer tous les articles

- **URL**: /posts
- **Méthode**: GET
- **Réponses**:
  - **Code 200** : succès. L'article a été supprimer.

  ```json
  {
    "posts": [
      {
        "title": "Le titre de l'article 1",
        "content": "Le contenu de l'articles 1",
        "image": "/images/post/640b3e9f8c19b6ab27d0a4f7-1678571607381.png",
        "user": {
            "_id": "640b3e9f8c19b6ab27d0a4f7",
            "username": "John Doe",
            "avatar": "/images/avatar/640b3e9f8c19b6ab27d0a4f7-1678565697344.png",
            "id": "640b3e9f8c19b6ab27d0a4f7"
        },
        "createdAt": "2023-03-11T21:53:27.412Z",
        "updatedAt": "2023-03-11T21:53:27.412Z",
        "id": "640cf8577c0eb6052ecacfca"
    },
    {
        "title": "Le titre de l'article 2 ",
        "image": null,
        "user": {
            "username": "Jane DOe",
            "avatar": null,
            "id": "640b45dd41c9815ce57eefb8"
        },
        "createdAt": "2023-03-12T12:24:43.402Z",
        "updatedAt": "2023-03-12T12:24:43.402Z",
        "id": "640dc48be3c3433a45b83e4c"
      }
    ]
  }
  ```

---

### 5.2: Le Model

1. Créer le schema d'un article':
    - title: String
    - content: String
    - image: String
    - createdAt
    - updatedAt

2. Créer le model pour Article et l'exporter.

---

### 5.3: Les controlleurs

1. Ajouter un article

    Créer la fonction pour ajouter un articl

    - Récuperer le token des headers.
    - Verifier si il est correcte, sinon retourner un erreur
    - Reçoit dans le body de la requête: title, content et image.
    - Tester si les données existent et sont correctes, sinon retourner des message d'erreurs dans la réponse.
    - utliser multer pour enregistrer l'image de l'article
    - Utilise le Model Article pour ajouter l'article dans la base de données et repondre avec un message de réussite.

2. Récuperer les articles d'un utilisateur

    Créer la fonction pour récuperer les articles d'un utilisateur

    - Récuperer le token des headers.
    - Verifier si il est correcte, sinon retourner un erreur
    - Utilise le Model Article pour récuperer les articles d'un utilisateur
    - Repondre avec la liste d'articles

3. Récuperer tout les articles

    Créer la fonction pour récuperer touts les articles
    - Utilise le Model Article pour récuperer tous les articles
    - Repondre avec la liste d'articles

---

## Etape 6: Les articles dans le Front end ***TERMINER***

### 6.1 Ajouter des articles

Créer le composant formulaire pour ajouter un article.

1. Le formulaire comporte: title, content et un champs pour ajouter une image
2. Tester les entrées de l'utilisateur.
3. Envoyer la requête avec les données et le fichier pour l'image.
4. Gerer les reponse de la backend.

---

### 6.2 Afficher les articles de l'utilisateur

Utiliser le  contexte pour récuperer les articles de l'utilisateur et les afficher.

---

### 6.3 Afficher tous les articles

Dans la page de accueil, utiliser un useEffect pour récuperer tous les articles et les afficher.
