// Librairie pour créer et lancer un serveur HTTP
import express from "express";

// Librairie pour intéragir avec une BDD MongoDB
import mongoose from "mongoose";

// Librairie pour afficher les reqûetes réçues dans la console
import morgan from "morgan";

// Routeur pour les requête sur /users: authentification, informations
import { usersRouter } from "./src/routes/users.js";

// Routeur pour les requête sur /posts: créer, supprimer récuperer des posts
import { postsRouter } from "./src/routes/posts.js";

// Le port sur lquel le serveur et lancé
const PORT = 3001;

// L'URI de la base de données
const MONGOURI = "mongodb://127.0.0.1:27017/blog";

//Créer une instance de l'objet express
const app = express();

//Les middleware
// Pour afficher les requêtes dans la console
app.use(morgan("combined"));

// Parse le JSON dans le body des requêtes
app.use(express.json());

// Parse les formulaure dans le body des reqêtes
app.use(express.urlencoded({ extended: true }));

// Servir statiquement les images: avatar et posts
app.use("/images", express.static("images"));

// Regrouper les routes de lu'itlisateur
app.use("/users", usersRouter);

// Regrouper les routes pour les articles
app.use("/posts", postsRouter);

// Connecter la base de donnée: asynchrone dont utilisation du then
mongoose.connect(MONGOURI).then(() => {
	// Afficher dans la console que la BDD est connecté
	console.log("Connected do DB");

	// Lancer le serveur sur le port sépcifié plus haut
	app.listen(PORT, () => {
		console.log("Serveur lancé sur le port : " + PORT);
	});
});
