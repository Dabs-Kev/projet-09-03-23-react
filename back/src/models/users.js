import mongoose from "mongoose";

const userSchema = mongoose.Schema({
	email: {
		type: String,
		required: true,
		unique: true,
	},
	username: {
		type: String,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	avatar: {
		type: String,
	},
	posts: [{ type: mongoose.Schema.Types.ObjectId, ref: "Post" }],
});

export const User = mongoose.model("User", userSchema);

export async function userDataReponse(user) {
	await user.populate("posts");
	return {
		id: user.id,
		email: user.email,
		username: user.username,
		avatar: user.avatar ? `/images/avatar/${user.avatar}` : null,
		posts: [
			...user.posts.map(({ title, content, _id, image, createdAt, updatedAt }) => ({
				title,
				content,
				id: _id,
				image: image ? `/images/post/${image}` : null,
				createdAt,
				updatedAt,
				user: { id: user.id },
			})),
		],
	};
}