// Bibiothèque pour hasher les mots de passes
import bcrypt from "bcrypt";

/**
 * Fonction qui prend un mot de passe en entrée et retourne une promesse qui se résout avec le mot de passe haché.
 * @param {string} password - Le mot de passe à hacher.
 * @returns {Promise<string>} - La promesse se résout avec le mot de passe haché.
 */
export async function hash(password) {
	// Utilisation de la méthode pour hacher le mot de passe avec un "salt" de 10 tours.
	return await bcrypt.hash(password, 10);
}

/**
 * Fonction qui prend un mot de passe et un mot de passe haché en entrée et retourne une promesse qui se résout avec un booléen indiquant si les mots de passe correspondent.
 * @param {string} password - Le mot de passe en clair à comparer.
 * @param {string} hashedPassword - Le mot de passe haché à comparer.
 * @returns {Promise<boolean>} - La promesse se résout avec un booléen indiquant si les mots de passe correspondent.
 */
export async function compare(password, hashedPassword) {
	// Utilisation de la méthode pour comparer le mot de passe en clair avec le mot de passe haché.
	// Renvoi d'un booléen indiquant si les mots de passe correspondent.
	return await bcrypt.compare(password, hashedPassword);
}

// Création d'un objet "Hash" pour exporter les fonctions
const Hash = {
	hash,
	compare,
};

// Exportation de l'objet "Hash"
export default Hash;
