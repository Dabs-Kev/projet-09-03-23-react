// Libraire native pour manipuler les fichier
import fs from "fs";

// Libraire native pour manipuler les chemins
import path from "path";
import { fileURLToPath } from "url";

/**
 * Fonction qui supprime un fichier situé dans le dossier du script en cours d'exécution.
 * @param {string} fileRelativePath - Le chemin relatif du fichier à supprimer par rapport au dossier du script en cours d'exécution.
 */
export const deleteFile = async (fileRelativePath) => {
	// Récupération du chemin absolu du fichier en utilisant l'URL du module actuel.
	const __filename = fileURLToPath(import.meta.url);
	const __dirname = path.dirname(__filename);
	const filePath = path.join(__dirname, `${fileRelativePath}`);

	// Tester si le fichier existe avant de le supprimer
	fs.access(filePath, fs.constants.F_OK, (err) => {
		if (err) {
			return;
		}

		// Supprimer le fichier
		fs.unlink(filePath, (err) => {
			return;
		});
	});
};
