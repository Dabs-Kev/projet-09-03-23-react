// Libraire pour créer et gérer les jwt token
import jwt from "jsonwebtoken";

// Définition de la clé secrète pour la signature et la vérification des JWT
const SECRET_KEY = "secret_key";

/**
 * Fonction qui prend un objet "payload" en entrée et retourne un JWT signé contenant ces informations.
 * @param {object} payload - L'objet JSON à signer dans le JWT.
 * @returns {string} - Le JWT signé.
 */
export function sign(payload) {
	// Utilisation de la méthode "jwt.sign" pour signer le "payload"
	// avec la clé secrète "SECRET_KEY" et une expiration de 7 jours.
	return jwt.sign(payload, SECRET_KEY, { expiresIn: "7d" });
}

/**
 * Fonction qui prend un JWT en entrée et une fonction de rappel "callback" qui sera appelée avec l'erreur ou le payload une fois que la vérification est terminée.
 * @param {string} tokenHeader - Le JWT à vérifier.
 * @param {function} callback - La fonction de rappel qui sera appelée une fois la vérification terminée.
 */
export function verify(tokenHeader, callback) {
	// Suppression de la partie "Bearer " de l'en-tête du jeton (si elle est présente).
	const token = tokenHeader.replace("Bearer ", "");

	// Utilisation de la méthodepour vérifier la validité du jeton en utilisant la clé.
	jwt.verify(token, SECRET_KEY, (err, payload) => {
		// Appel de la fonction de rappel avec l'erreur ou le payload une fois la vérification terminée.
		callback(err, payload);
	});
}

// Création d'un objet "JWT" pour exporter les fonctions.
const JWT = {
	sign,
	verify,
};

// Exportation de l'objet "JWT" comme l'export par défaut du module.
export default JWT;
