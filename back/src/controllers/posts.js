import { deleteFile } from "../libs/files.js";
import { Post } from "../models/posts.js";
import { User, userDataReponse } from "../models/users.js";

/**
 * Poster un article. La fonction réçoit le payload grâce au middleware verifyToken, le fichier image optionnel grâce au middleware formData.singl('image') et le titre et le contenu dans le body.
 * Apres aboir testé les entrées, les données de l'article sont enregistrées dans la base de données et renvoie dans les données de l'utilisateur avec le nouveau post.
 * Le controlleur repond avec 400 si le titre ou le contenu n'ont pas été fournis
 *
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @param {string} req.payload - Les données récuperer dans le token.
 * @param {string} req.file- Les données de l'image enregistrée.
 * @param {string} req.body.title - L'email de Le titre de l'article à ajouter.
 * @param {string} req.body.content - Le contenu de l'article à ajouter.
 * @returns {Response} - La réponse JSON avec status 200 et un message de réussite ou status 400 et un message d'erreur.
 *
 * @example
 * POST /posts/post [{payload, file, title, content}] -> 200 - {"user" : { ... }}
 * POST /posts/post [{payload, file, content}] -> 400 - {"message":"Title required"}
 */
export async function addPost(req, rep) {
  // récuperer l'ID depuis le payload mis par le middleware verifyToken dans la requete
	const { id } = req.payload;

  // récuperer les dpnnées de l'article dans le body de la requête
	const { title, content } = req.body;

  //reponse 400 si le titre n'est pas fournit dans la requête
  if(!title) return rep.status(400).json({message :"Title required"})

  //reponse 400 si le contenu n'est pas fournit dans la requête

  if(!content) return rep.status(400).json({message :"Content required"})

  // récuperer l'utilisateut depuis la BDD avec sont id
	const user = await User.findById(id);

  // Créer un nouveau post
	const newPost = new Post({ title, content, user, image: req.files[0]?.filename });

  // Ajouté la référence du nouvel article dans la liste de posts de l'utilisateur
	user.posts.push(newPost);

  // Enregistrer le post dans la BDD
	await newPost.save();

  // Mettre à jour l'utilisateur dans la BDD
	await user.save();

  // Remplir la liste de posts avec les données
	await user.populate("posts");

  //Reponse 200 avec les données de l'utilisateur contenant les nouveau post
	rep.status(200).json({ user: await userDataReponse(user) });
}

/**
 * Recupérer tous les articles de tous les utilisateurs
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @returns {Response} - La réponse JSON avec status 200 avec tous les articles.
 * @example
 * GET /posts -> 200 - {"posts":[ ... ]}
 */
export async function getPosts(req, rep) {
  
  // récuperer tous les posts avec le username et l'avatar de l'auteur du post
	const posts = await Post.find().populate({ path: "user", select: "username avatar" });

  // Réponse 200 avec tous les posts 
	rep.status(200).json({
		posts: posts.map((post) => ({
			...post._doc,
			image: post._doc.image ? `/images/post/${post._doc.image}` : null,
			id: post._doc._id,
			user: {
				...post._doc.user._doc,
				id: post._doc.user._doc._id,
				avatar: post._doc.user._doc.avatar
					? `/images/avatar/${post._doc.user._doc.avatar}`
					: null,
			},
		})),
	});
}

/**
 * Supprimer un article avec son id.
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @param {Object} req.payload - Les données du token récuperer et mis dans la requête par * le middleware verifyToken.
 * @param {Object} req.params.id - L'ID de l'article a supprimer (obligatoire)
 * @returns {Response} - La réponse JSON avec status 200 et les données de l'utilisateur.
 * @example
 * GET /posts -> 200 - {"posts":[ ... ]}
 */
export const deletePost = async (req, rep) => {
	// récuperer l'id de l'utilisateur depuis le payload mis grâce au middleware verifyToken
  const { id } = req.payload;

  // récuperer l'id de l'article a supprimer
	const { id: postId } = req.params;

  // récuperer l'utilisateur et sa liste de posts
	const user = await User.findById(id).populate({ path: "posts", select: "_id image" });

  // Créer un tableau de boolean pour savoir quel article supprimer avec l'id
	const filteredPosts = await Promise.all(
		user.posts.map(async (post) => {
      // Si l'id correspond
			if (post.id === postId) {
				// Supprimer le post de la BDD
        await Post.findByIdAndDelete(postId);
				
        // Si le post posseder une image
        if (post.image) {
          // Supprimer le fichier image
					await deleteFile(`../../images/post/${post.image}`);
				}
        //Retourner false pour enlever le postde la list de posts de l'utilisateur
				return false;
			}
      // Retourner true pour garder le post dans le filter
			return true;
		})
	);

  // Filtré la liste de posts de l'utilisateur en utilisant le tableau de boolean
	user.posts = user.posts.filter((_, i) => filteredPosts[i]);

  // Mettre a jour l'utilisateur dans la BDD
	user.save();

  //Reponse avec status 200 et les nouvel données de l'utilisateur
	rep.status(200).json({ user: await userDataReponse(user) });
};
