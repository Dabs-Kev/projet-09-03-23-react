// Model pour la base de données, et
// une fonction pour formatter l'utilsateur dans les réponses
import { User, userDataReponse } from "../models/users.js";

// Méthodes pour hasher et conparer les mot de passe
import Hash from "../libs/hash.js";

// méthodes pour signer et verifier les jwt tokens
import JWT from "../libs/jwt.js";

// Fonction pour supprimer les images de post
import { deleteFile } from "../libs/files.js";

/**
 * @function getUserProfil
 * @description Récupère les informations d'un utilisateur avec son ID depuis la base de données et renvoie une réponse formatée.
 *
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @param {string} req.params.id - L'ID de l'utilisateur à rechercher.
 * @returns {Response} - La réponse JSON avec status 200 et les informations de l'utilisateur formatée ou status 404 et un message d'erreur.
 *
 * @example
 * GET /users/1234 -> 200 - {"user":{ ... }}
 * GET /users/unknown -> 404 - {"message":"User not found"}
 */
export async function getUserProfil(req, rep) {
	// Récupérer l'ID de l'utilisateur à partir des paramètres de la requête.
	const { id } = req.params;

	// Rechercher l'utilisateur avec son ID dans la base de données.
	const user = await User.findById(id);

	// Si l'utilisateur n'est pas trouvé, renvoyer une réponse 404.
	if (!user) return rep.status(404).json({ message: "User not found" });

	// Renvoyer une réponse 200 avec les informations formatées de l'utilisateur.
	rep.status(200).json({ user: await userDataReponse(user) });
}

/**
 * @function addUser
 * @description Inscription de l'utilisateur. La fonction réçoit l'email, le username et le mot de passe dnas le body.
 * Apres aboir testé les entrées, les donnée sont enregistrées dans la base de données et renvoie dans la réponse une message de réussite.
 *
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @param {string} req.body.email - L'email de l'utilisateur à ajouter.
 * @param {string} req.body.username - Le username de l'utilisateur à ajouter.
 * @param {string} req.body.password - Le password de l'utilisateur à ajouter.
 * @returns {Response} - La réponse JSON avec status 200 et un message de réussite ou status 404 et un message d'erreur.
 *
 * @example
 * POST /users/signup [{email, password, username}] -> 200 - {"message":"User added"}
 * POST /users/signup [{password, username}] -> 400 - {"message":"Email required"}
 */
export async function addUser(req, rep) {
	// récupère les données de la requête
	const { email, username, password } = req.body;

	// vérifie si l'email est présent dans la requête
	if (!email) {
		// renvoie une réponse HTTP avec un code d'erreur 400 et un message
		rep.status(400).json({ message: "Email required" });
		return;
	}

	// vérifie si le mot de passe est présent dans la requête
	if (!password) {
		// renvoie une réponse HTTP avec un code d'erreur 401 et un message
		rep.status(401).json({ message: "Password required" });
		return;
	}

	// vérifie si le nom d'utilisateur est présent dans la requête
	if (!username) {
		// renvoie une réponse HTTP avec un code d'erreur 401 et un message
		rep.status(401).json({ message: "Username required" });
		return;
	}

	// récupère l'utilisateur existant avec le même email depuis la base de données
	const existingUser = await User.findOne({ email });

	// vérifie si l'utilisateur existe déjà
	if (existingUser) {
		// renvoie une réponse HTTP avec un code d'erreur 401 et un message
		rep.status(401).json({ message: "User already exist" });
		return;
	}

	// hache le mot de passe avec le coût 10
	const hashedPassword = await Hash.hash(password, 10);

	// crée un nouvel utilisateur avec l'email, le nom d'utilisateur et le mot de passe haché
	const newUser = await User.create({ email, username, password: hashedPassword });

	// renvoie une réponse HTTP avec un code de réussite 200 et un message indiquant que l'utilisateur a été ajouté
	rep.status(200).json({ message: "User added" });
}

/**
 * @function loginUser
 * @description Connexion de l'utilisateur: Le client fournit dans le body de la requête un email et un mot de passe, retourne 401 si omis.
 * Récuperer les données depuis la BDD, et retourner 404 si l'utilisateur n'est pas trouvé.
 * Comporer les mot de passe et retourner 401 si les il ne sont pas identiques.
 * Créer un token et le retrouné 200 avec les données de l'utilisateur formatées et le token
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @param {string} req.body.email - L'email de l'utilisateur à authentifié.
 * @param {string} req.body.password - Le password de l'utilisateur à authentifié.
 * @returns {Response} - La réponse JSON avec status 200 et les données de l'utilsateur et un token ou status 404 et un message d'erreur.
 *
 * @example
 * POST /users/login [{email, password}] -> 200 - {"user":{ ... }, "token":"..."}
 * POST /users/login [{password}] -> 400 - {"message":"Email required"}
 */
export async function loginUser(req, rep) {
	// récupère les données de la requête
	const { email, password } = req.body;

	//Errur 401 Si pas d'email fournit
	if (!email) return rep.status(401).json({ message: "Email required" });

	//Erreur 401 Si pas de password fournit
	if (!password) return rep.status(401).json({ message: "Password required" });

	//Récuperer user dpuis la DB
	const user = await User.findOne({ email });

	//Erreur 404 Si l'utilisateur n'existe pas
	if (!user) return rep.status(404).json({ message: "Email doesnt exist" });

	//Comparer le mot de passe avec celui de la base de donnée
	const isMatch = await Hash.compare(password, user.password);

	// Erreu 401 si les mot de passes ne sont pas identiques
	if (!isMatch) {
		rep.status(401).json({ message: "Passwords does not match" });
		return;
	}

	// Créer le tocken avec l'id de l'utilisateur dedans
	const token = JWT.sign({ id: user._id });

	// Reponse avec status 200, le token et les données de l'utilisateur formatées
	rep.status(200).json({
		user: await userDataReponse(user),
		token: token,
	});
}

/**
 * @function loginUser
 * @description Récupere les données de l'utilisateur avec l'ID fournit dans les paramètre de l'url.
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @param {string} req.params.id - L'ID de l'utilisateur à récuperer.
 * @returns {Response} - La réponse JSON avec status 200 et les données de l'utilsateur ou status 404 et un message d'erreur.
 *
 * @example
 * POST /users/login [{email, password}] -> 200 - {"user":{ ... }, "token":"..."}
 * POST /users/login [{password}] -> 400 - {"message":"Email required"}
 */
export async function getUser(req, rep) {
	// récuperer les l'ID depuis la paramètre de la requete
	const { id } = req.payload;

	//Récuperer user dpuis la DB
	const user = await User.findById(id);

	// Si l'utilisateur n'est pas trouvé, envoyer une réponse 404.
	if (!user) return rep.status(404).json({ message: "User not found" });

	//reponse 200 avec les données de l'utilisateur formatées
	rep.status(200).json({ user: await userDataReponse(user) });
}

/**
 * @function updateUsername
 * @description Cette fonction gère la mise à jour du pseudo d'un utilisateur en utilisant l'ID réçu dans le payload du token, et le nouveau pseudo réçu dans le body de la requpete. et repond avec un status 200 et les nouvelles données de l'utilisateur
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @param {string} req.payload.id - L'ID de l'utilisateur mis par le middleware verifyToken dans la requete.
 * @param {string} req.body.username - Le nouveau pseudo de l'utilisateur
 * @returns {Response} - La réponse JSON avec status 200 et les données de l'utilsateur.
 *
 * @example
 * PUT /users/username [{payload, username}] -> 200 - {"user":{ ... }}
 */
export async function updateUsername(req, rep) {
	// récuperer l'ID depuis le payload mis par le middleware verifyToken dans la requete
	const { id } = req.payload;

	// récuperer le nouveau pseudo de l'utilisateur dans le body de la requête
	const { username: newUsername } = req.body;

	// Mettre à jour et récuperer les données de l'utilisateur
	const user = await User.findByIdAndUpdate(id, { username: newUsername }, { new: true });

	// reponse avec status 200 et les données de l'utilisateur formatées
	rep.status(200).json({ user: await userDataReponse(user) });
}

/**
 * @function uploadAvatar
 * @description Cette fonction gère la mise à jour de l'avatar d'un utilisateur. Elle reçoit l'id dans req.payload et le nom du fichier image enrégistée plus tot dans req.file.filename.
 * Elle supprime l'ancien avatar si il existe et repond avec les nouvelles données de l'utilisateur formatées
 * @param {Object} req - L'objet de requête Express.
 * @param {Object} rep - L'objet de réponse Express.
 * @param {string} req.payload.id - L'ID de l'utilisateur mis par le middleware verifyToken dans la requete.
 * @param {string} req.file - L'es information du fichier image enregistré plus tot par le middleware formData.single('avatar').
 * @returns {Response} - La réponse JSON avec status 200 et les données de l'utilsateur.
 *
 * @example
 * POST /users/avatar [{token, file}] -> 200 - {"user":{ ... }}
 */
export async function uploadAvatar(req, rep) {
	// récuperer l'ID depuis le payload mis par le middleware verifyToken dans la requete
	const { id } = req.payload;

	//Récuperer user dpuis la DB
	const user = await User.findById(id);

	//Verifier si l'utilisateur avait un avatar
	if (user.avatar) {
		// Supprimer l'ancien avatar
		await deleteFile(`../../images/avatar/${user.avatar}`);
	}

	// récuperer et mettre à jour avatar avec le nom du fichier enregisté
	user.avatar = req.file.filename;

	//Enregistrer l'utilisateur dans la BDD
	await user.save();

	// Reponse 200 avec les nouvelle données de l'utilisateur
	rep.status(200).json({ user: await userDataReponse(user) });
}
