// Librairie qui fournit des middleware pour gérer les données des formulaire
import multer from "multer";

// Configuration du stockage des fichiers en utilisant multer.
const storage = multer.diskStorage({
	// Définition du dossier de destination en fonction du chemin de requête.
	destination: function (req, file, cb) {
		cb(null, `./images${req.path}`);
	},
	filename: function (req, file, cb) {
		// Définition du nom de fichier en fonction de l'ID de l'utilisateur, de la date et de l'extension.
		const extension = file.originalname.split(".").pop();
		const newFileName = `${req.payload.id}-${Date.now()}.${extension}`;
		cb(null, newFileName);
	},
});

/**
 * Middleware multer qui gère les formulaires avec des fichiers envoyés.
 * @param {Object} storage - La configuration du stockage pour les fichiers envoyés.
 */
export const formMiddleware = multer({ storage });
