// Importer les methodes pour gérer les jwt token
import JWT from "../libs/jwt.js";

/**
 * Middleware pour vérifier le token d'authentification envoyé dans la requête.
 * @param {Object} req - La requête HTTP.
 * @param {Object} rep - La réponse HTTP.
 * @param {Function} next - La fonction suivante à appeler dans la chaîne de middleware.
 */
export function verifyTokenMiddleware(req, rep, next) {
	// Récupération de l'en-tête Authorization de la requête.
	const authorization = req.headers.authorization;

	// Vérification de la présence du token d'authentification.
	if (!authorization) {
		return rep.status(401).json({ message: "access_token required" });
	}

	// Vérification de la validité du token d'authentification en utilisant la librairie JWT.
	JWT.verify(authorization, (err, payload) => {
		// Si le token est invalide, renvoyer une réponse d'erreur.
		if (err) {
			return rep.status(401).json({ message: "access_token invalid" });
		}
		// ajouter les informations du payload à la requête
		req.payload = payload;

		// appeler la fonction suivante dans la chaîne de middleware.
		next();
	});
}
