import express from "express";

// Les controlleurs pour chaque route
import {
	addUser,
	getUser,
	getUserProfil,
	loginUser,
	updateUsername,
	uploadAvatar,
} from "../controllers/users.js";

// Middleware pour la gestion de formulaire et upload d'avatar
import { formMiddleware } from "../middlewares/form.js";

// Middleware pour la gestion du token dans les route securisées
import { verifyTokenMiddleware } from "../middlewares/jwt.js";

// Déclaration de l'instance du routeur
export const usersRouter = express.Router();

// Route pour l'inscription de l'utilisateur
usersRouter.post("/signup", formMiddleware.array(), addUser);

// Route pour la connexion, fournitteste les entréer et fournit un jwt token
usersRouter.post("/login", formMiddleware.array(), loginUser);

// Route pour récuperer les infos de l'utilisateur, passe pa validation du token
usersRouter.get("/", verifyTokenMiddleware, formMiddleware.array(), getUser);
usersRouter.get("/:id", getUserProfil);

// Route pour mettre à jour le pseudo de l'utilisateur, passe pa validation du token
usersRouter.put(
	"/username",
	verifyTokenMiddleware,
	formMiddleware.array(),
	updateUsername
);

// Route pour changer l'image de profil, passe par la validation du token et
// Le middleware formMiddleware pour l'enregistrement de l'image
usersRouter.post(
	"/avatar",
	verifyTokenMiddleware,
	formMiddleware.single("avatar"),
	uploadAvatar
);
