import express from "express";

// Les controlleurs pour chaque route
import { addPost, deletePost, getPosts } from "../controllers/posts.js";

// Middleware pour la gestion de formulaire et upload d'image d'un post
import { formMiddleware } from "../middlewares/form.js";

// Middleware pour la gestion du token dans les route securisées
import { verifyTokenMiddleware } from "../middlewares/jwt.js";

// Déclaration de l'instance du routeur
export const postsRouter = express.Router();

// Route pour la création d'article, passe par la validation du token
postsRouter.post(
	"/post",
	verifyTokenMiddleware,
	formMiddleware.array("image", 1),
	addPost
);

// Route pour la suppression d'article, passe par la validation du token
postsRouter.delete("/:id", verifyTokenMiddleware, deletePost);

// Route pour récuperer tous les articles
postsRouter.get("/", getPosts);
