import { BrowserRouter } from "react-router-dom";
import UserProvider from "./components/contexts/UserProvider";
import Footer from "./components/layouts/Footer/Footer";
import Main from "./components/layouts/Main/Main";
import Navbar from "./components/layouts/Navbar/Navbar";
function App() {
	return (
		<div>
			<UserProvider>
				<BrowserRouter>
					<Navbar />
					<Main />
					<Footer />
				</BrowserRouter>
			</UserProvider>
		</div>
	);
}

export default App;
