const TOKEN_KEY = "token";

export async function getToken() {
	return localStorage.getItem(TOKEN_KEY);
}

export async function setToken(token) {
	return localStorage.setItem(TOKEN_KEY, token);
}

export async function removeToken() {
	return localStorage.removeItem(TOKEN_KEY);
}
const Stockage = {
	getToken,
	setToken,
	removeToken,
};

export default Stockage;
