import axios from "axios";
export async function post(route, body, config) {
	try {
		const { status, data } = await axios.post(route, body, config);
		return { status, data };
	} catch (err) {
		const { status, data } = err.response;
		return { status, data };
	}
}

export async function get(route, config) {
	try {
		const { status, data } = await axios.get(route, config);
		return { status, data };
	} catch (err) {
		const { status, data } = err.response;
		return { status, data };
	}
}

export async function put(route, body, config) {
	try {
		const { status, data } = await axios.put(route, body, config);
		return { status, data };
	} catch (err) {
		const { status, data } = err.response;
		return { status, data };
	}
}

export async function remove(route, body, config) {
	try {
		const { status, data } = await axios.delete(route, config);
		return { status, data };
	} catch (err) {
		const { status, data } = err.response;
		return { status, data };
	}
}

const Requests = {
	post,
	get,
	put,
	remove,
};

export default Requests;
