export function validateEmail(email) {
	const validated = email.includes("@");
	const error = validated ? "" : "L'email est incorrect!";
	return { validated, error };
}

export function validateUsername(username) {
	const validated = username !== "";
	const error = validated ? "" : "Pseudo ne peut pas être vide!";
	return { validated, error };
}

export function validatePassword(password) {
	const validated = password >= 6;
	const error = validated ? "" : "Le mot de passe est trop court!";
	return { validated, error };
}

export function validateConfirmPassword(password, confirmPassword) {
	const validated = password === confirmPassword;
	const error = validated ? "" : "Les mots de passes ne sont pas identiques!";
	return { validated, error };
}
export function validateTitle(title) {
	const validated = title !== "";
	const error = validated ? "" : "Le titre est obligatoire!";
	return { validated, error };
}
export function validateContent(content) {
	const validated = content !== "";
	const error = validated ? "" : "Le contenu de l'article est obligatoire!";
	return { validated, error };
}

const Validators = {
	email: validateEmail,
	username: validateUsername,
	password: validatePassword,
	confirmPassword: validateConfirmPassword,
	title: validateTitle,
	content: validateContent,
};

export default Validators;
