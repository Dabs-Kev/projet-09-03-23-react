import { useContext } from "react";
import { UserContext } from "../../contexts/UserProvider";
import Button from "../../ui/Button/Button";
import classes from "./ListArticles.module.css";
import defaultAvatar from "../../../assets/defaultAvatar.png";
export default function ListArticles({ posts, updatePosts }) {
	//Inverser la liste de posts pour afficher les plus récent d'abord
	const listPost = [...posts].reverse();

	const { user, deleteArticle } = useContext(UserContext);

	//Fonction pour formatter la date
	function formatDate(postDate) {
		const date = new Date(postDate);
		const localDate = date.toLocaleDateString("fr-FR", {
			weekday: "long",
			year: "numeric",
			month: "long",
			day: "numeric",
			hour: "2-digit",
			minute: "2-digit",
		});
		return localDate;
	}

	//Supprimer un article et mettre à jour la liste
	const deletePost = (id) => {
		deleteArticle(id);
		updatePosts && updatePosts();
	};
	return (
		<div className={classes.container}>
			{listPost.map((post) => (
				<div className={classes.postContainer} key={post.id}>
					<div className={classes.authorContainer}>
						<img
							className={classes.avatar}
							alt={"Avatar de l'auteur"}
							src={
								post?.user?.avatar
									? post.user.avatar
									: user.avatar
									? user.avatar
									: defaultAvatar
							}
						/>
						<p className={classes.username}>
							{post.user?.username ? post.user.username : user.username}
						</p>
					</div>
					<h3>{post.title}</h3>
					{post.image && (
						<img className={classes.image} src={post.image} alt={post.title} />
					)}
					<p className={classes.content}>{post.content}</p>
					<p className={classes.date}>{formatDate(post.createdAt)}</p>
					{user && user.id === post.user?.id ? (
						<Button type="danger" onClick={() => deletePost(post.id)}>
							{"Supprimer le post"}
						</Button>
					) : null}
				</div>
			))}
		</div>
	);
}
