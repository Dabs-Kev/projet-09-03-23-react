import React, { useContext, useState } from "react";
import InputWithError from "../../ui/InputWithError/InputWithError";
import Button from "../../ui/Button/Button";
import TextArea from "../../ui/TextArea/TextArea";
import { UserContext } from "../../contexts/UserProvider";
import classes from "./ArticleForm.module.css";
import defaultArticle from "../../../assets/defaultArticle.jpg";
import Validators from "../../../libs/validators";
export default function ArticleForm() {
	const { postArticle } = useContext(UserContext);

	const [title, setTitle] = useState("");
	const [titleError, setTitleError] = useState();

	const [content, setContent] = useState("");
	const [contentError, setContentError] = useState();

	const [file, setFile] = useState(null);
	const [previewUrl, setPreviewUrl] = useState(null);

	const [responseMessage, setResponseMessage] = useState("");

	function handleTitle(e) {
		setTitleError("");
		setTitle(e.target.value);
	}

	function handeContent(e) {
		setContentError("");
		setContent(e.target.value);
	}

	const handleImage = async (e) => {
		const selectedFile = e.target.files[0];
		setFile(selectedFile);
		console.log(selectedFile);
		setPreviewUrl(URL.createObjectURL(selectedFile));
	};
	const post = async () => {
		// Validations
		const titleValidation = Validators.title(title);
		const contentValidation = Validators.content(content);

		if (titleValidation.validated && contentValidation.validated) {
			const reponse = await postArticle(title, content, file);
			if (reponse.status !== 200) {
				setResponseMessage("Un probléme est survenu, veuillez réessayer plus tard!");
				return;
			}
			setResponseMessage("Votre article à été posté!");
			setPreviewUrl(null);
			setFile(null);
			setContentError("");
			setTitleError("");

			return;
		}
		setTitleError(titleValidation.error);
		setContentError(contentValidation.error);
	};
	return (
		<div className="container">
			<InputWithError
				placeholder={"Entrez un titre"}
				value={title}
				onChange={handleTitle}
				error={titleError}
				type="text"
			/>

			<label htmlFor="image-upload">
				{previewUrl ? (
					<>
						<img className={classes.image} src={previewUrl} alt="File preview" />
					</>
				) : (
					<img className={classes.image} alt="article" src={defaultArticle} />
				)}

				<input onChange={handleImage} id="image-upload" type="file" />
			</label>

			<TextArea
				placeholder={"De quoi voulez-vous parler aujourd'hui ?"}
				onChange={handeContent}
				error={contentError}
				value={content}
			/>
			<Button onClick={post}>Poster</Button>
			<p>{responseMessage}</p>
		</div>
	);
}
