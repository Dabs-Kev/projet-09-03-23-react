import React, { useContext, useState } from "react";
import Validators from "../../../libs/validators";
import { UserContext } from "../../contexts/UserProvider";
import Button from "../../ui/Button/Button";
import InputWithError from "../../ui/InputWithError/InputWithError";

export default function SignupForm() {
	const { signup } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [emailError, setEmailError] = useState(null);

	const [username, setUsername] = useState("");
	const [usernameError, setUsernameError] = useState(null);

	const [password, setPassword] = useState("");
	const [passwordError, setPasswordError] = useState(null);

	const [confirmPassword, setConfirmPassword] = useState("");
	const [confirmPasswordError, setConfirmPasswordError] = useState(null);

	const [responseMessage, setResponseMessage] = useState("");

	function handleEmail(e) {
		setEmailError("");
		setEmail(e.target.value);
	}

	function handleUsername(e) {
		setUsernameError("");
		setUsername(e.target.value);
	}

	function handlePassword(e) {
		setPasswordError("");
		setPassword(e.target.value);
	}

	function handleConfirmPassword(e) {
		setConfirmPassword("");
		setConfirmPassword(e.target.value);
	}

	async function signupUser(e) {
		e.preventDefault();
		setEmailError("");
		setUsernameError("");
		setPasswordError("");
		setConfirmPasswordError("");

		// Validations
		const emailValidation = Validators.email(email);
		const usernameValidation = Validators.username(username);
		const passwordValidation = Validators.password(password);
		const confirmPasswordValidation = Validators.confirmPassword(
			password,
			confirmPassword
		);
		if (
			emailValidation.validated &&
			usernameValidation.validated &&
			passwordValidation.validated &&
			confirmPasswordValidation.validated
		) {
			const reponse = await signup(email, username, password);
			if (reponse.status !== 200) {
				setResponseMessage("Vous êtes déja inscrit, connectez-vous plutot!");
				return;
			}
			setResponseMessage("Inscription réussie!");
			return;
		}

		setEmailError(emailValidation.error);
		setUsernameError(usernameValidation.error);
		setPasswordError(passwordValidation.error);
		setConfirmPasswordError(confirmPasswordValidation.error);
	}
	return (
		<div style={{ width: "100%" }}>
			<h2>Inscription</h2>
			<form>
				<InputWithError
					placeholder="Email"
					type="email"
					value={email}
					onChange={handleEmail}
					error={emailError}
				/>
				<InputWithError
					placeholder="Pseudo"
					type="text"
					value={username}
					onChange={handleUsername}
					error={usernameError}
				/>
				<InputWithError
					placeholder="Mot de passe"
					type="password"
					value={password}
					onChange={handlePassword}
					error={passwordError}
				/>
				<InputWithError
					placeholder="Confirmez votre mot de passe"
					type="password"
					value={confirmPassword}
					onChange={handleConfirmPassword}
					error={confirmPasswordError}
				/>
				<Button onClick={signupUser}>Inscription</Button>
				<p>{responseMessage}</p>
			</form>
		</div>
	);
}
