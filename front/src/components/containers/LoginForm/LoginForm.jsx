import React, { useContext, useState } from "react";
import Validators from "../../../libs/validators";
import Button from "../../ui/Button/Button";
import InputWithError from "../../ui/InputWithError/InputWithError";
import { UserContext } from "../../contexts/UserProvider";
export default function LoginForm() {
	const { login } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [emailError, setEmailError] = useState(null);
	const [password, setPassword] = useState("");
	const [passwordError, setPasswordError] = useState(null);

	const [responseMessage, setResponseMessage] = useState("");

	function handleEmail(e) {
		setEmail(e.target.value);
	}

	function handlePassword(e) {
		setPassword(e.target.value);
	}

	async function loginUser(e) {
		e.preventDefault();

		const emailValidation = Validators.email(email);
		const passwordValidation = Validators.password(password);

		if (emailValidation.validated && passwordValidation.validated) {
			const reponse = await login(email, password);
			if (!reponse) return;

			if (reponse.status === 404) {
				setResponseMessage("L'email n'existe pas! Veuillez vous inscrire d'abord!");
				return;
			}

			if (reponse.status === 401) {
				setResponseMessage("L'email ou/et le mot de passe n'est pas bon!");
				return;
			}
		}

		setEmailError(emailValidation.error);
		setPasswordError(passwordValidation.error);
	}
	return (
		<div style={{ width: "100%" }}>
			<h2>Connexion</h2>
			<form>
				<InputWithError
					placeholder="Email"
					type="email"
					value={email}
					onChange={handleEmail}
					error={emailError}
				/>
				<InputWithError
					placeholder="Mot de passe"
					type="password"
					value={password}
					onChange={handlePassword}
					error={passwordError}
				/>
				<Button onClick={loginUser}>Se connecter</Button>
				<p>{responseMessage}</p>
			</form>
		</div>
	);
}
