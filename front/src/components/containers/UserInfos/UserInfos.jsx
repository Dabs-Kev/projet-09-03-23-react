import { useContext, useState } from "react";
import { UserContext } from "../../contexts/UserProvider";
import defaultAvatar from "../../../assets/defaultAvatar.png";
import classes from "./UserInfos.module.css";
import EditableText from "../../ui/EditableText/EditableText";
import Validators from "../../../libs/validators";
import { CancelIcon, CheckIcon } from "../../ui/Icons/Icons";
export default function UserInfos() {
	const { user, updateUsername, updateAvatar } = useContext(UserContext);
	const [file, setFile] = useState(null);
	const [previewUrl, setPreviewUrl] = useState(null);
	const updateUser = async (username) => {
		await updateUsername(username);
	};

	const previewAvatar = async (e) => {
		console.log(e);
		const selectedFile = e.target.files[0];
		setFile(selectedFile);
		setPreviewUrl(URL.createObjectURL(selectedFile));
	};

	const cancel = () => {
		setPreviewUrl(null);
		setFile(null);
	};

	const updateUserAvatar = async () => {
		const reponse = await updateAvatar(file);
		setPreviewUrl(null);
		setFile(null);
	};
	return (
		<div className={classes.container}>
			<EditableText validator={Validators.username} onValidate={updateUser}>
				{user.username}
			</EditableText>
			<div>
				<label htmlFor="file-upload">
					{previewUrl ? (
						<>
							<img className={classes.avatar} src={previewUrl} alt="File preview" />
						</>
					) : (
						<img
							className={classes.avatar}
							alt="Avatar de l'utilisateur"
							src={user.avatar ? user.avatar : defaultAvatar}
						/>
					)}

					<input onChange={previewAvatar} id="file-upload" type="file" />
				</label>
				{previewUrl && (
					<div className={classes.iconsContainer}>
						<div className={classes.icon} onClick={updateUserAvatar}>
							<CheckIcon />
						</div>
						<div className={classes.icon} onClick={cancel}>
							<CancelIcon />
						</div>
					</div>
				)}
			</div>
		</div>
	);
}
