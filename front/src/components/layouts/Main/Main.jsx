import classes from "./Main.module.css";
import { Route, Routes, Navigate } from "react-router-dom";
import Home from "../../pages/Home/Home";
import Auth from "../../pages/Auth/Auth";
import Profil from "../../pages/Profil/Profil";
import { useContext } from "react";
import { UserContext } from "../../contexts/UserProvider";

export default function Main() {
	const { user } = useContext(UserContext);
	return (
		<div className={classes.main}>
			<Routes>
				<Route exact path="/" element={<Home />} />
				<Route path="/auth" element={user ? <Navigate to={"/profil"} /> : <Auth />} />
				<Route path="/profil" element={!user ? <Navigate to={"/auth"} /> : <Profil />} />
			</Routes>
		</div>
	);
}
