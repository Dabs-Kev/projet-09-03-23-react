import React from "react";
import classes from "./Footer.module.css";
export default function Footer() {
	return (
		<footer className={classes.footer}>
			Exemple de projet FullStack avec Reactjs et ExpressJS par Djemai Samy.
		</footer>
	);
}
