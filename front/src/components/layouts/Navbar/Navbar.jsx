import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import classes from "./Navbar.module.css";
import menuLogo from "../../../assets/menu.svg";
import { UserContext } from "../../contexts/UserProvider";
export default function Navbar() {
	const { user, logout } = useContext(UserContext);

	const [isMenuHidden, setIsMenuHidden] = useState(true);

	const toggleMenu = () => {
		setIsMenuHidden(!isMenuHidden);
	};

	const logoutUser = () => {
		logout();
	};

	return (
		<nav className={classes.container}>
			<div>
				<img
					onClick={toggleMenu}
					className={classes.menuLogo}
					src={menuLogo}
					alt="Bouton pour ouvrir et fermer le menu de la navbar"
				/>
			</div>
			<ul className={classes.links} style={{ left: isMenuHidden ? "-100%" : "0px" }}>
				<li className={classes.link}>
					<Link to="/">Accueuil</Link>
				</li>
				{user ? (
					<>
						<li className={classes.link}>
							<Link to="/profil">Profil</Link>
						</li>
						<li
							className={classes.link}
							style={{ cursor: "pointer" }}
							onClick={logoutUser}
						>
							<p>Se deconnecter</p>
						</li>
					</>
				) : (
					<li className={classes.link}>
						<Link to="/auth">Se connecter</Link>
					</li>
				)}
			</ul>
		</nav>
	);
}
