import React, { useState } from "react";
import LoginForm from "../../containers/LoginForm/LoginForm";
import SignupForm from "../../containers/SignupForm/SignupForm";

export default function Auth() {
	const [isLogin, setIsLogin] = useState(true);

	function toggleLogin() {
		setIsLogin(!isLogin);
	}
	return (
		<div className="container">
			{isLogin ? <LoginForm /> : <SignupForm />}
			<p className="link" onClick={toggleLogin}>
				{isLogin ? "Pas encore membre? Inscrivez vous!" : "Deja membre? Connectez-vous!"}
			</p>
		</div>
	);
}
