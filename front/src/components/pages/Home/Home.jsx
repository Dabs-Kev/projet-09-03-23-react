import { useEffect, useState } from "react";
import Requests from "../../../libs/requests";
import ListArticles from "../../containers/ListArticles/ListArticles";
export default function Home() {
	const [posts, setPosts] = useState([]);
	const [update, setUpdate] = useState(true);
	const getPosts = async () => {
		const reponse = await Requests.get("/posts");
		setPosts(reponse.data.posts);
	};

	const toggleUpdate = () => {
		setUpdate(!update);
	};
	useEffect(() => {
		getPosts();
	}, [update]);
	console.log(posts);
	return (
		<div>
			<ListArticles posts={posts} updatePosts={toggleUpdate} />
		</div>
	);
}
