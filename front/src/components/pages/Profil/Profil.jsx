import React, { useContext } from "react";
import ArticleForm from "../../containers/ArticleForm/ArticleForm";
import ListArticles from "../../containers/ListArticles/ListArticles";
import UserInfos from "../../containers/UserInfos/UserInfos";
import { UserContext } from "../../contexts/UserProvider";

export default function Profil() {
	const { user } = useContext(UserContext);
	return (
		<div>
			<UserInfos />
			<ArticleForm />
			<ListArticles posts={user.posts} />
		</div>
	);
}
