import classes from "./TextArea.module.css";
export default function TextArea({ children, placeholder, value, error, onChange }) {
	return (
		<>
			{" "}
			<textarea
				placeholder={placeholder}
				className={classes.textarea}
				onChange={onChange}
				value={value}
			></textarea>
			<p className={classes.error}>{error}</p>
		</>
	);
}
