import Cancel from "./Cancel";
import Check from "./Check";

export const CheckIcon = Check;

export const CancelIcon = Cancel;

const Icons = {
	CheckIcon,
	CancelIcon,
};
export default Icons;
