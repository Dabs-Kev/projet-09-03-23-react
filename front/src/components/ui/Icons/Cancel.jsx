import React from "react";

export default function Cancel() {
	return (
		<div>
			<svg
				style={{ width: "90%", fill: "red" }}
				viewBox="0 0 512 512"
				fill="none"
				xmlns="http://www.w3.org/2000/svg"
			>
				<g clipPath="url(#clip0_1115_78)">
					<rect
						x="-10.1938"
						y="444.691"
						width="641.76"
						height="106.163"
						rx="23"
						transform="rotate(-45 -10.1938 444.691)"
					/>
					<rect
						x="64.875"
						y="-9.10156"
						width="641.76"
						height="106.163"
						rx="23"
						transform="rotate(45 64.875 -9.10156)"
					/>
				</g>
			</svg>
		</div>
	);
}
