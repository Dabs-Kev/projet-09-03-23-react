import classes from "./Button.module.css";

export default function Button({ children, onClick, type }) {
	let color = "black";
	switch (type) {
		case "danger":
			color = "red";
			break;
		default:
			color = "black";
	}
	return (
		<div className={classes.container}>
			<button
				style={{ backgroundColor: color }}
				className={classes.button}
				onClick={onClick}
			>
				{children}
			</button>
		</div>
	);
}
