import classes from "./EditableText.module.css";
import { useState } from "react";
import InputWithError from "../InputWithError/InputWithError";
import { CancelIcon, CheckIcon } from "../Icons/Icons";
export default function EditableText({ children, validator, onValidate }) {
	const [isEditing, setIsEditing] = useState(false);
	const [text, setText] = useState(children);
	const [error, setError] = useState(null);

	function handleTextChange(e) {
		setText(e.target.value);
	}
	function toggleEditing() {
		setIsEditing(!isEditing);
		setText(children);
		setError(null);
	}

	function validate() {
		const validation = validator(text);
		if (validation.validated) {
			toggleEditing();
			onValidate(text);
			return;
		}

		setError(validation.error);
	}

	return (
		<div>
			{isEditing ? (
				<form className={classes.formContainer}>
					<div className={classes.inputContainer}>
						<InputWithError
							placeholder={children}
							value={text}
							onChange={handleTextChange}
							error={error}
						/>
					</div>
					<div className={classes.iconsContainer}>
						<div className={classes.icon} onClick={validate}>
							<CheckIcon />
						</div>
						<div className={classes.icon} onClick={toggleEditing}>
							<CancelIcon />
						</div>
					</div>
				</form>
			) : (
				<div onClick={toggleEditing}>
					<h2>{children}</h2>
				</div>
			)}
		</div>
	);
}
