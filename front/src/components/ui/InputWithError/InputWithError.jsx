import classes from "./InputWithError.module.css";
export default function InputWithError({ placeholder, type, value, onChange, error }) {
	return (
		<>
			<input
				className={classes.input}
				placeholder={placeholder}
				value={value}
				onChange={onChange}
				type={type}
			/>
			<p className={classes.error}>{error}</p>
		</>
	);
}
