import { createContext, useEffect, useState } from "react";
import Requests from "../../libs/requests";
import Stockage from "../../libs/stockage";
export const UserContext = createContext();

export default function UserProvider({ children }) {
	/**
	 * Fonction pour récuperer les données de l'utilisateur si le token est dans le storage.
	 * Cette fonction met à jour le contexte avec les données reçues,
	 * cela aura pour effet de rediriger l'utilisateur sur la page de profil (Composant Main).
	 * Si une erreur survient dans la réponse elle enleve le token du storage.
	 * @return {void}
	 */
	async function getUser() {
		// Récuperer le token du localStorage
		const localToken = await Stockage.getToken();

		// Arreter la fonction si pas de token
		if (!localToken) {
			return;
		}

		//Envoyer une requete pour récuperer les données
		const reponse = await Requests.get("/users", {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localToken}`,
			},
		});

		// Si erreur enlever le token du storage.
		if (reponse.status !== 200) {
			Storage.removeToken();
			return;
		}

		// Si tout va bien, mettre les données dans le contexte
		// Cela aura pour effet de rediriger l'utilisateur sur la page de profil
		setUser(reponse.data.user);
		return;
	}

	/**
	 * Fonction pour connecter l'utilisateur en envoyant une requête sur /users/login.
	 * Cette fonction met à jour le contexte avec les données reçues et le storage avec le token,
	 * Cela aura pour effet de rediriger l'utilisateur sur la page de profil (Composant Main).
	 * Si un erreyr survient dans la réponse, la fonction retourne la réponse pour la gestion d'erreur dans le formulaire LoginForm
	 * @param {String} email Email à envoyer à la backend
	 * @param {String} password Le mot de passe  à envoyer à la backend
	 * @return {Response | null}
	 */
	async function login(email, password) {
		// Envoyer une requete avec email et password
		const reponse = await Requests.post(
			"/users/login",
			{ email, password },
			{
				headers: {
					"Content-Type": "application/json",
				},
			}
		);

		// Si tout va bien, stocker les données pour rediriger l'utilisateur vers la page de profil.
		if (reponse.status === 200) {
			// Metttre à jour le contexte
			setUser(reponse.data.user);
			// Stocker le jwt token dans le storage
			await Stockage.setToken(reponse.data.token);
			// Arreter la fonction car l'utilisateur sera rediriger vers la page de profil
			return;
		}

		// Sinon, Retourner la reponse pour la gestion d'erreurs dans les formulaire
		return reponse;
	}

	/**
	 * Fonction pour inscrire l'utilisateur en envoyant une requête sur /users/signup.
	 * Cette fonction prends les entrées tester de l'utilisateur: email, username et mot de passe et retoune la réponse reçue
	 * @param {String} email Email à envoyer à la backend
	 * @param {String} username Pseudo à envoyer à la backend
	 * @param {String} password Le mot de passe  à envoyer à la backend
	 * @return {Response | null}
	 */
	async function signup(email, username, password) {
		// Envoyer une requete avec email username  et password
		const reponse = await Requests.post(
			"/users/signup",
			{ email, username, password },
			{
				headers: {
					"Content-Type": "application/json",
				},
			}
		);
		// Retourner la reponse au formulaire SingupForm
		return reponse;
	}

	/**
	 * Fonction pour déconnecter l'utilisateur et enlevant le token du storage,
	 * et en mettant la variables d'état user a null, ce qui provoquera un redirection
	 * @return {Response | null}
	 */
	async function logout() {
		await Stockage.removeToken();
		setUser(null);
	}

	/**
	 * Fontion pour mettre à jour le pseudo de l'utilisateur
	 * @return {Response | null}
	 */
	async function updateUsername(username) {
		// Récuperer le token du localStorage
		const localToken = await Stockage.getToken();

		// Arreter la fonction si pas de token
		if (!localToken) {
			return;
		}

		// Envoyer une requete avec username
		const reponse = await Requests.put(
			"/users/username",
			{ username },
			{
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localToken}`,
				},
			}
		);

		// Si erreur enlever le token du storage.
		if (reponse.status === 200) {
			console.log(reponse);
			setUser(reponse.data.user);
			return;
		}
		return reponse;
	}

	/**
	 * Fontion pour mettre à jour l'avatar de l'utilisateur
	 * @return {Response | null}
	 */
	async function updateAvatar(avatar) {
		// Récuperer le token du localStorage
		const localToken = await Stockage.getToken();

		// Arreter la fonction si pas de token
		if (!localToken) {
			return;
		}

		const formData = new FormData();
		formData.append("avatar", avatar);

		// Envoyer une requete avec username
		const reponse = await Requests.post("/users/avatar", formData, {
			headers: {
				"Content-Type": "multipart/form-data",
				Authorization: `Bearer ${localToken}`,
			},
		});

		// Si erreur enlever le token du storage.
		if (reponse.status === 200) {
			console.log(reponse);
			setUser(reponse.data.user);
			return;
		}
		return reponse;
	}

	/**
	 * Fontion pour poster un article
	 * @return {Response | null}
	 */
	async function postArticle(title, content, image) {
		// Récuperer le token du localStorage
		const localToken = await Stockage.getToken();

		// Arreter la fonction si pas de token
		if (!localToken) {
			return;
		}

		const formData = new FormData();
		formData.append("title", title);
		formData.append("content", content);
		formData.append("image", image);

		// Envoyer une requete avec username
		const reponse = await Requests.post("/posts/post", formData, {
			headers: {
				"Content-Type": "multipart/form-data",
				Authorization: `Bearer ${localToken}`,
			},
		});

		// Si erreur enlever le token du storage.
		if (reponse.status === 200) {
			setUser(reponse.data.user);
		}
		return reponse;
	}

	/**
	 * Fontion pour suppriler un article
	 * @return {Response | null}
	 */
	async function deleteArticle(id) {
		// Récuperer le token du localStorage
		const localToken = await Stockage.getToken();
		// Arreter la fonction si pas de token
		if (!localToken) {
			return;
		}
		console.log(localToken);
		// Envoyer une requete avec username
		const reponse = await Requests.remove(
			`/posts/${id}`,
			{},
			{
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localToken}`,
				},
			}
		);

		// Si erreur enlever le token du storage.
		if (reponse.status === 200) {
			setUser(reponse.data.user);
		}
		return reponse;
	}

	// Variable d'état pour stocker les données et les partager à d'autre composants grâce au contexte
	const [user, setUser] = useState(null);

	// Récuperer les données quand le composant est rendu
	useEffect(() => {
		console.log("ml");
		getUser();
	}, []);
  console.log(user);

	// Stocker la variables et les fonctions nécessaire dnas le contexte
	// Puis afficher les enfant du provider dans children
	return (
		<UserContext.Provider
			value={{
				user,
				login,
				signup,
				logout,
				updateUsername,
				updateAvatar,
				postArticle,
				deleteArticle,
			}}
		>
			{children}
		</UserContext.Provider>
	);
}
